(function ($) {
  Drupal.behaviors.Opr2FieldEndoscCdai = {
    attach: function(context) {
      $('form.node-form').delegate('div.group-endosc-cdai :input', 'change', function(){
        var cdai = 0,
          $loose = $('#edit-field-endosc-cdai-loose-und-0-value'),
          $abdpain = $('#edit-field-endosc-cdai-abdpain-und'),
          $welbein = $('#edit-field-endosc-cdai-welbein-und'),
          $comorb = $('#edit-field-endosc-cdai-comorb-und :input'),
          $obstip = $('#edit-field-endosc-cdai-obstip-und :input'),
          $abdmass = $('#edit-field-endosc-cdai-abdmass-und'),
          $htoc = $('#edit-field-endosc-cdai-htoc-und-0-value'), htoc, htocMean,
          $height = $('#edit-field-height-und-0-value'),
          $weight = $('#edit-field-weight-und-0-value'),
          $idealwt = $('#edit-field-endosc-cdai-idealwt-und-0-value');

        if ($loose.val()) {
          cdai += parseInt($loose.val()) * 2;
        }
        if (parseInt($abdpain.val())) {
          cdai += parseInt($abdpain.val()) * 5;
        }
        if (parseInt($welbein.val())) {
          cdai += parseInt($welbein.val()) * 7;
        }
        if ($comorb.filter(':checked').length) {
          cdai += $comorb.filter(':checked').length * 20;
        }
        if ($obstip.filter(':checked[value=1]').length) {
          cdai += 30;
        }
        if (parseInt($abdmass.val())) {
          cdai += parseInt($abdmass.val()) < 2 ? 20 : 50;
        }
        if ($htoc.val()) {
          htoc = parseFloat($htoc.val());
          if ($('#edit-field-patient-und-form-field-patient-gender-und :input[value=N]:checked').length) {
            htocMean = 42;
          }
          else {
            htocMean = 47;
          }
          cdai += (htocMean - htoc) * 6;
        }
        if ($weight.val() && $idealwt.val()) {
          cdai += 100 - Math.floor(100 * parseFloat($weight.val()) / parseFloat($idealwt.val()));
        }

        $('#edit-field-endosc-cdai-und-0-value').val(cdai);
      });
    },
  };
})(jQuery);

