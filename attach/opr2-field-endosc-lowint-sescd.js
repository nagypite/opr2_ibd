(function ($) {
  Drupal.behaviors.Opr2FieldEndoscLowintSESCD = {
    attach: function(context) {
      var $selects = $("\
          #edit-field-endosc-ilcol-il-siz-und,\
          #edit-field-endosc-ilcol-il-uls-und,\
          #edit-field-endosc-ilcol-il-afs-und,\
          #edit-field-endosc-ilcol-il-nar-und,\
          #edit-field-endosc-ilcol-rt-siz-und,\
          #edit-field-endosc-ilcol-rt-uls-und,\
          #edit-field-endosc-ilcol-rt-afs-und,\
          #edit-field-endosc-ilcol-rt-nar-und,\
          #edit-field-endosc-ilcol-tv-siz-und,\
          #edit-field-endosc-ilcol-tv-uls-und,\
          #edit-field-endosc-ilcol-tv-afs-und,\
          #edit-field-endosc-ilcol-tv-nar-und,\
          #edit-field-endosc-ilcol-lt-siz-und,\
          #edit-field-endosc-ilcol-lt-uls-und,\
          #edit-field-endosc-ilcol-lt-afs-und,\
          #edit-field-endosc-ilcol-lt-nar-und,\
          #edit-field-endosc-ilcol-re-siz-und,\
          #edit-field-endosc-ilcol-re-uls-und,\
          #edit-field-endosc-ilcol-re-afs-und,\
          #edit-field-endosc-ilcol-re-nar-und \
      "),
        $score = $('#edit-field-endosc-lowint-sescd-und-0-value');

      $selects.change(function(){
        var score = 0, currVal;
        $selects.each(function() {
          currVal = Number($(this).val());
          if (currVal) {
            score += Number(currVal);
          }
        });
        $score.val(score);
      });

      $score.attr('readonly', true);
    },
  };
})(jQuery);

