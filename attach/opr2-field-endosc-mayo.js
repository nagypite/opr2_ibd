(function ($) {
  Drupal.behaviors.Opr2FieldEndoscMayo = {
    attach: function(context) {
    return;
      $('form.node-form').delegate('#edit-field-height-und-0-value, #edit-field-weight-und-0-value', 'change', function(){
        var height = parseFloat($('#edit-field-height-und-0-value').val().replace(',','.')),
        weight = parseInt($('#edit-field-weight-und-0-value').val()), bmi;

        if (height && weight) {
          bmi = weight / height / height;
          $('#edit-field-bmi-und-0-value').val(bmi.toFixed(2).toString().replace('.', ','));
        }
      });
    },
  };
})(jQuery);

