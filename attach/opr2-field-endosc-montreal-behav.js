(function ($) {
  Drupal.behaviors.Opr2FieldEndoscMontrealBehav = {
    attach: function(context) {
      $('form.node-form').delegate('#edit-field-endosc-montreal-behav-und', 'change', function(){
        var behav = $(this).val();
        $('.group-endosc-cdai').toggle(behav == '1' || behav == '2');
        $('.group-endosc-pdai, .group-endosc-fist').toggle(behav == '3');
      });
      $('#edit-field-endosc-montreal-behav-und').change();
    },
  };
})(jQuery);

