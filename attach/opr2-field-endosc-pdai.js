(function ($) {
  Drupal.behaviors.Opr2FieldEndoscPdai = {
    attach: function(context) {
      $('form.node-form').delegate('div.group-endosc-pdai :input', 'change', function(){
        var pdai = 0, selVal;

        $('div.group-endosc-pdai select').each(function() {
          selVal = parseInt($(this).val());
          if (selVal) {
            pdai += selVal;
          }
        });

        $('#edit-field-endosc-pdai-und-0-value').val(pdai);
      });
    },
  };
})(jQuery);


