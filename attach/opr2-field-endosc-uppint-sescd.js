(function ($) {
  Drupal.behaviors.Opr2FieldEndoscUppintSESCD = {
    attach: function(context) {
      var $selects = $("\
        #edit-field-endosc-oeduo-oe-siz-und,\
        #edit-field-endosc-oeduo-oe-uls-und,\
        #edit-field-endosc-oeduo-oe-afs-und,\
        #edit-field-endosc-oeduo-oe-nar-und,\
        #edit-field-endosc-oeduo-st-siz-und,\
        #edit-field-endosc-oeduo-st-uls-und,\
        #edit-field-endosc-oeduo-st-afs-und,\
        #edit-field-endosc-oeduo-st-nar-und,\
        #edit-field-endosc-oeduo-du-siz-und,\
        #edit-field-endosc-oeduo-du-uls-und,\
        #edit-field-endosc-oeduo-du-afs-und,\
        #edit-field-endosc-oeduo-du-nar-und \
      "),
        $score = $('#edit-field-endosc-uppint-sescd-und-0-value');

      $selects.change(function(){
        var score = 0, currVal;
        $selects.each(function() {
          currVal = Number($(this).val());
          if (currVal) {
            score += Number(currVal);
          }
        });
        $score.val(score);
      });

      $score.attr('readonly', true);
    },
  };
})(jQuery);

