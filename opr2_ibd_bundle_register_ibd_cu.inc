<?php

function opr2_ibd_bundle_register_ibd_cu() {
  $bundle = array(
    'machine_name' => 'register_ibd_cu',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => array(array('value'=>'v1')),
      ),
    ), // end instances

    'groups' => array(
    ), // end groups

    'tree' => array(
      'field_form_version',

      'group_opr_data' => array(
        'field_hospital',
        'field_opr_doctor_code',
        'field_blood_code',
        'field_blood_date',
      ),
      'group_personal' => array(
        'field_patient',
        'field_patient_race2',
        'field_patient_bloodtype',
        'field_patient_bloodtype_rh',
        'field_interview_date',
        'field_consent_form',
      ),
      'group_checkin' => array(
        'field_checkin_type',
        'field_checkin_admission',
        'field_checkin_discharge',
        'field_checkin_days',
      ),
      'group_inline' => array(
        'group_risk' => array(
          'group_alcohol' => array(
            'field_alcohol_consumption',
            'field_alcohol_freq',
            'field_alcohol_amount2',
            'field_alcohol_since2',
            'field_alcohol_2w_amount',
            'group_alcohol_past' => array(
              'field_alcohol_past',
              'field_alcohol_past_freq',
              'field_alcohol_past_amount',
              'field_alcohol_past_years',
              'field_alcohol_past_ended2',
            ),
          ),
          'group_smoking' => array(
            'field_smoking',
            'field_smoking_amount2',
            'field_smoking_since2',
            'group_smoking_past' => array(
              'field_smoking_past',
              'field_smoking_past_amount',
              'field_smoking_past_years',
              'field_smoking_past_ended2',
            ),
          ),
          'group_coffee' => array(
            'field_coffee',
            'field_coffee_amount',
          ),
          'group_drugs' => array(
            'field_drug_consumption',
            'field_drug_name',
            'field_drug_amount',
            'field_drug_since3',
          ),
          'group_diabetes' => array(
            'field_diabetes',
            'field_diabetes_type',
            'field_diabetes_since',
          ),
          'group_birthcont' => array(
            'field_birthcont',
            'field_birthcont_when',
          ),
          'group_appendect' => array(
            'field_appendect',
            'field_appendect_date',
          ),
          'group_fam' => array(
            'field_fam',
            'field_fam_uc',
            'field_fam_uc_rel',
            'field_fam_cd',
            'field_fam_cd_rel',
          ),
          'field_quals',
          'group_exam_tpmt' => array(
            'field_exam_tpmt',
            'field_exam_tpmt_res',
          ),
          'group_ai' => array(
            'field_ai',
            'field_ai_dg',
          ),
        ), // end group_risk

        'group_dg' => array(
          'field_dg_new',
          'field_dg_type',
          'field_dg_date',
          'field_dg_compstart',

          'group_dg_posex' => array(
            'field_dg_posex_sym',
            'field_dg_posex_bioch',
            'field_dg_posex_endosc',
            'field_dg_posex_hyst',
            'group_dg_posex_imag' => array(
              'field_dg_posex_imaguh',
              'field_dg_posex_imagct',
              'field_dg_posex_imagmr',
            ),
          ),
        ), // end group_dg

        'group_sym' => array(
          'group_abdpain' => array(
            'field_abdpain',
            'field_stoma_since',
            'field_abdpain_type',
            'field_abdpain_strength',
            'field_stoma_loc',
            'field_stoma_loc_detail',
            'field_stoma_rad',
          ),
          'group_abdpain_general' => array( // register_cancer_form_a
            'field_abdpain_general',
            'field_abdpain_general_behav2',
          ),
          'group_nausea' => array(
            'field_nausea',
          ),
          'group_vomiting' => array(
            'field_vomiting',
            'field_vomiting_times2',
            'field_vomiting_contents2',
          ),
          'group_fever' => array(
            'field_fever',
            'field_fever_since2',
            'field_fever_amount2',
            'field_fever_amount_rect',
          ),
          'group_appetite' => array(
            'field_appetite',
          ),
          'group_weightloss' => array(
            'field_weightloss',
            'field_weightloss_weeks2',
            'field_weightloss_amount2',
          ),
          'group_stool' => array(
            'field_stool',
          ),
          'group_sym_extint' => array(
            'group_eyesym' => array(
              'field_eyesym',
              'field_eyesym_type',
            ),
            'group_skinsym' => array(
              'field_skinsym',
              'field_skinsym_type',
            ),
            'group_jointsym' => array(
              'field_jointsym',
              'field_jointsym_peri',
              'field_jointsym_axi',
            ),
            'group_thremb' => array(
              'field_thremb',
              'field_thremb_date',
              'field_thremb_loc',
              'field_thremb_type',
            ),
            'field_kidstonpain',
            'field_galstonpain',
          ),
        ), // end group_sym

        'group_bioch' => array(
          'group_bioch_stool' => array(
            'group_bioch_stool_bact' => array(
              'field_bioch_stool_bact',
              'field_bioch_stool_bact_res',
            ),
            'group_bioch_stool_para' => array(
              'field_bioch_stool_para',
              'field_bioch_stool_para_res',
            ),
            'group_bioch_stool_clostr' => array(
              'field_bioch_stool_clostr',
              'field_bioch_stool_clostr_res',
            ),
            'group_bioch_stool_calpr' => array(
              'field_bioch_stool_calpr',
              'field_bioch_stool_calpr_res',
            ),
          ),
          'group_bioch_lab' => array(
            'group_lab_results_opt' => array(
              'field_lab_result_esr',

              'field_lab_result_wbc',
              'field_lab_result_rbc',
              'field_lab_result_hg',
              'field_lab_result_htoc',
              'field_lab_result_mcv',
              'field_lab_result_throm',

              'field_lab_result_na',
              'field_lab_result_k',
              'field_lab_result_ca',
              'field_lab_result_mg',
              'field_lab_result_p',
              'field_lab_result_cl',
              'field_lab_result_fe',

              'field_lab_result_gluc',
              'field_lab_result_amilase',
              'field_lab_resut_lipase',

              'field_lab_result_urean',
              'field_lab_result_creat',
              'field_lab_result_egfr2',

              'field_lab_result_bilitot',
              'field_lab_result_dcbili',
              'field_lab_result_asatgot',
              'field_lab_result_alatgpt',
              'field_lab_result_ggt',
              'field_lab_result_alph',
              'field_lab_result_ldh',
              'field_lab_result_prot',
              'field_lab_result_protinr',
              'field_lab_result_fibr',

              'field_lab_result_chol',
              'field_lab_result_trig',
              'field_lab_result_urac',
              'field_lab_result_tsh',

              'field_lab_result_totprot',
              'field_lab_result_alb',
              'field_lab_result_globa1',
              'field_lab_result_globa2',
              'field_lab_result_globb',
              'field_lab_result_globg',

              'field_lab_result_crp2',
              'field_lab_result_procalc2',
              'field_lab_result_iga',
              'field_lab_result_igm',
              'field_lab_result_igg',
              'field_lab_result_igg4',
              'field_lab_result_cyafst',
              'field_lab_result_cya2hr',
              'field_lab_result_cea',
              'field_lab_result_ca199',
              'field_lab_result_intl',
              'field_lab_result_adlmb',
              'field_lab_result_ada',
              'field_lab_result_b12',
              'field_lab_result_folac',
              'field_lab_result_vvt6tgn',
              'field_lab_result_ascaiga',
              'field_lab_result_ascaigg',
              'field_lab_result_ana',
              'field_lab_result_panca',

              'field_lab_result_pao2',
              'field_lab_result_hco3',
              'field_lab_result_so2',

              'field_lab_result_swcl',
              'field_lab_result_uramil',
              'field_lab_result_urlip',
              'field_lab_result_urcreat',
            ),
          ),
        ),
        'group_endosc' => array(
          'group_endosc_montreal' => array(
            'field_endosc_montreal',
          ),

          'group_endosc_mayo' => array(
            'field_endosc_mayo_stool',
            'field_endosc_mayo_rectbl',
            'field_endosc_mayo_mucous',
            'field_endosc_mayo_doc',
            'field_endosc_mayo',
          ),
        ),

        'group_hyst' => array(
          'field_hyst_type',
          'field_hyst_desc',
        ),

        'group_imaging' => array(
          'group_imaging_abdus' => array(
            'field_imaging_abdus',
            'field_imaging_abdus_res',
          ),
          'group_imaging_abdrtg' => array(
            'field_imaging_abdrtg',
            'field_imaging_abdrtg_res',
          ),
          'group_imaging_abdct' => array(
            'field_imaging_abdct',
            'field_imaging_abdct_ind',
            'field_imaging_abdct_res',
            'group_imaging_abdct_ent' => array(
              'field_imaging_abdct_ent',
              'field_imaging_abdct_ent_res',
            ),
          ),
          'group_imaging_abdmr' => array(
            'field_imaging_abdmr',
            'field_imaging_abdmr_ind',
            'field_imaging_abdmr_res',
            'group_imaging_abdmr_ent' => array(
              'field_imaging_abdmr_ent',
              'field_imaging_abdmr_ent_res',
            ),
          ),
          'group_imaging_mrcp' => array(
            'field_imaging_mrcp',
            'field_imaging_mrcp_ind',
            'field_imaging_mrcp_res',
          ),
          'group_imaging_eus' => array(
            'field_imaging_eus',
            'field_imaging_eus_ind',
            'field_imaging_eus_res',
          ),
        ),

        'group_exam' => array(
          'group_functexam' => array(
            'field_functexam',
            'group_functexam_lact' => array(
              'field_functexam_lact',
              'field_functexam_lact_res',
            ),
            'group_functexam_lactu' => array(
              'field_functexam_lactu',
              'field_functexam_lactu_res',
            ),
            'group_functexam_oth' => array(
              'field_functexam_oth',
              'field_functexam_oth_type',
              'field_functexam_oth_res',
            ),
          ),
          'group_gentest_pre' => array(
            'field_gentest_pre',
            'field_gentest_pre_res',
          ),
        ),

        'group_int' => array(
          'group_int_surg' => array(
            'field_int_surg',
            'group_int_ibd' => array(
              'field_int_ibd',
              'field_int_ibd_timing',
              'group_int_ibd_ind' => array(
                'group_int_ibd_indfulcol' => array(
                  'field_int_ibd_indfulcol',
                  'field_int_ibd_indfulcol_date',
                ),
                'group_int_ibd_indperf' => array(
                  'field_int_ibd_indperf',
                  'field_int_ibd_indperf_date',
                ),
                'group_int_ibd_indbleed' => array(
                  'field_int_ibd_indbleed',
                  'field_int_ibd_indbleed_date',
                ),
                'group_int_ibd_indthfail' => array(
                  'field_int_ibd_indthfail',
                  'field_int_ibd_indthfail_date',
                ),
                'group_int_ibd_inddyspl' => array(
                  'field_int_ibd_inddyspl',
                  'field_int_ibd_inddyspl_date',
                ),
                'group_int_ibd_indcarc' => array(
                  'field_int_ibd_indcarc',
                  'field_int_ibd_indcarc_date',
                ),
                'group_int_ibd_indthres' => array(
                  'field_int_ibd_indthres',
                  'field_int_ibd_indthres_date',
                ),
                'group_int_ibd_indobst' => array(
                  'field_int_ibd_indobst',
                  'field_int_ibd_indobst_date',
                ),
                'group_int_ibd_indfist' => array(
                  'field_int_ibd_indfist',
                  'field_int_ibd_indfist_date',
                ),
                'group_int_ibd_inddrain' => array(
                  'field_int_ibd_inddrain',
                  'field_int_ibd_inddrain_date',
                ),
                'group_int_ibd_indfulm' => array(
                  'field_int_ibd_indfulm',
                  'field_int_ibd_indfulm_date',
                ),
              ),
              'field_int_ibd_type',
              'field_int_ibd_reop',
              'field_int_ibd_comp',
              'field_int_ibd_rutg',
            ),
            'field_int_nonibd',
          ),
          'group_int_end' => array(
            'field_int_end',
            'field_int_end_dil',
            'field_int_end_seg',
            'field_int_end_type',
            'field_int_end_stent',
          ),
        ),

        'group_status' => array(
          'group_status_inline' => array(
              'field_bp',
              'field_bp_dias',
              'field_pulse',
              'field_weight',
              'field_height',
              'field_bmi',
              'field_resprate',
              'field_bodytemp',
              'field_abdomguard',

              'field_stoolweek',
              'field_stoolweekloose',
              'field_stoolappear',
              'field_stoolcons',
              'field_stoolhabit',
              'field_stoolnightdiarr',
              'group_abdcomp' => array(
                'field_abdcomp',
                'field_abdcomp_type',
                'field_abdcomp_since',
              ),
              'field_rectcomp',
              'field_nausea',
              'field_vomit',
              'field_swalcomp',
              'field_breathcomp',
              'field_chestcomp',
              'field_urgencomp',
              'field_conjuct',
              'field_oedema',
              'field_jaundice',
              'field_cyanosis',

              'group_chestexam' => array(
                'field_chestexam_lungknock',
                'field_chestexam_lungausc',
                'field_chestexam_heartrythm',
                'field_chestexam_heartsnd',
                'field_chestexam_lymphnode',
                'field_chestexam_hair',
                'field_chestexam_mouth',
                'field_chestexam_thyroid',
              ),
              'group_abdexam' => array(
                'group_abdomtender' => array(
                  'field_abdomtender',
                  'field_abdomtender_loc',
                ),
                'group_abdexam_resist' => array(
                  'field_abdexam_resist',
                  'field_abdexam_resist_loc',
                ),
                'field_abdexam_liver',
                'field_abdexam_spleen',
                'group_abdexam_rdv' => array(
                  'field_abdexam_rdv',
                  'field_abdexam_rdv_fist',
                  'field_abdexam_rdv_absc',
                ),
              ),
              'group_physdiff' => array(
                'field_physdiff_gen',
                'field_physdiff_art',
                'field_physdiff_vein',
                'field_physdiff_neur',
                'field_physdiff_ear',
              ),
          ),
        ),

        'group_ther' => array(
          'field_ther_med',

          'group_ther_bio' => array(
            'field_ther_bio',
            'field_ther_bio_med',
          ),
        ),

        'group_preg' => array(
          'field_preg',
          'group_preg1' => array(
            'field_preg_start',
            'field_preg_end',
            'field_preg_ectopic',
            'field_preg_weeks',
            'field_preg_cs',
            'field_preg_pre',
            'field_preg_weight',
            'field_preg_apgar',
            'field_preg_apgar10',
            'group_preg_conabn' => array(
              'field_preg_conabn',
              'field_preg_conabn_type',
            ),
            'field_preg_med',
          ),
          'field_preg_live',
        ), // end group_preg

        'group_comp' => array(
          'group_comp_int' => array(
            'field_comp_int',
            'field_comp_int_type',
          ),
          'group_comp_bildu' => array(
            'field_comp_bildu',
            'field_comp_bildu_type',
          ),
          'group_complication_orgfail' => array(
            'field_complication_orgfail',
            'field_complication_orgfail_org',
          ),
          'group_therapy_intensive' => array(
            'field_therapy_intensive',
            'field_therapy_intensive_desc2',
            'field_therapy_intensive_notes',
          ),
          'group_complication_death' => array(
            'field_complication_death',
            'field_complication_death_date',
            'field_complication_death_time',
          ),
          'group_comp_other' => array(
            'field_comp_other',
            'field_comp_other_type',
          ),
          'field_complication_notes',
        ), // end group_comp
      ),
    ), // end tree
  );

  return $bundle;
}

