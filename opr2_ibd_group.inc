<?php

function opr2_ibd_group($name, $bundle) {
  switch ($name) {
  case 'group_inline':
  case 'group_patient_inline':
    return _opr2grp('inline');
  case 'group_checkin':
    return _opr2grp('conditional', '', array(
      'col_map' => "7|2|Ha kórházi felvétel:\n4",
    ));
  case 'group_risk':
    return _opr2grp('category', '2. Rizikótényezők');
  case 'group_alcohol':
    return _opr2grp('conditional', '', array(
      'col_map' => "3\n2\n4\n6\n4",
    ));
  case 'group_alcohol_past':
    return _opr2grp('conditional', '');
  case 'group_smoking':
    return _opr2grp('conditional', '', array(
      'col_map' => "3\n6\n4",
    ));
  case 'group_smoking_past':
  case 'group_coffee':
  case 'group_drugs':
  case 'group_diabetes':
  case 'group_birthcont':
  case 'group_appendect':
  case 'group_fam':
  case 'group_ai':
    return _opr2grp('conditional', '');

  case 'group_dg':
    return _opr2grp('category', '3. Diagnózis');
  case 'group_dg_posex':
    return _opr2grp('indent', 'A diagnózist alátámasztó vizsgálat típusa (igen válasz jelölendő pozitív eltérés esetén)');
  case 'group_dg_posex_imag':
    return _opr2grp('div', '');

  case 'group_sym':
    return _opr2grp('category', '3.1. Kezdeti panaszok/tünetek');
  case 'group_abdpain':
  case 'group_abdpain_general':
  case 'group_nausea':
  case 'group_vomiting':
  case 'group_fever':
  case 'group_appetite':
  case 'group_weightloss':
  case 'group_stool':
    return _opr2grp('conditional', '');
  case 'group_sym_extint':
    return _opr2grp('div', 'Extraintestinális manifesztáció');
  case 'group_eyesym':
  case 'group_skinsym':
  case 'group_jointsym':
  case 'group_thremb':
    return _opr2grp('conditional', '');

  case 'group_bioch':
    return _opr2grp('category', '3.2. Biokémiai eltérések');
  case 'group_bioch_stool':
    return _opr2grp('category', '3.2.1. Széklet vizsgálatok');
  case 'group_bioch_stool_bact':
  case 'group_bioch_stool_para':
  case 'group_bioch_stool_clostr':
  case 'group_bioch_stool_calpr':
    return _opr2grp('conditional', '');
  case 'group_bioch_lab':
    return _opr2grp('category', '3.2.2. Szérum vizsgálatok');
  case 'group_lab_results_opt':
    return array (
      'label' => '',
      'format_type' => 'table',
      'format_settings' =>
      array (
        'instance_settings' =>
        array (
          'label_visibility' => '1',
          'empty_label_behavior' => '1',
          'table_row_striping' => 0,
          'always_show_field_label' => 0,
          'classes' => 'group-lab-results group-lab-results-opt field-group-table',
          'first_column' => '',
          'second_column' => '',
          'desc' => '',
        ),
      ),
    );

  case 'group_endosc':
    return _opr2grp('category', '3.3. Endoszkópia ');
  case 'group_endosc_ilcol':
    return _opr2grp('indent', 'Ileo-colonoscopia (SES-CD; simple endoscopic score for CD)');
  case 'group_endosc_ilcol_il':
    return _opr2grp('div', 'Ileum');
  case 'group_endosc_ilcol_rt':
    return _opr2grp('div', 'Jobb colonfél');
  case 'group_endosc_ilcol_tv':
    return _opr2grp('div', 'Transversum');
  case 'group_endosc_ilcol_lt':
    return _opr2grp('div', 'Bal colon');
  case 'group_endosc_ilcol_re':
    return _opr2grp('div', 'Rectum');
  case 'group_endosc_oeduo':
    return _opr2grp('indent', 'Oesophagogastroduodenoscopia (SES-CD)');
  case 'group_endosc_oeduo_oe':
    return _opr2grp('div', 'Nyelőcső');
  case 'group_endosc_oeduo_st':
    return _opr2grp('div', 'Gyomor');
  case 'group_endosc_oeduo_du':
    return _opr2grp('div', 'Duodenum');
  case 'group_endosc_cdai':
    return _opr2grp('indent', 'Luminális CD aktivitása: CDAI', array(
      'classes' => 'group-endosc-cdai',
    ));
  case 'group_endosc_fist':
    return _opr2grp('indent', 'Fistulázó CD', array(
      'classes' => 'group-endosc-fist',
    ));
  case 'group_endosc_fist_seton':
    return _opr2grp('conditional', '');
  case 'group_endosc_pdai':
    return _opr2grp('indent', 'Fistulázó CD aktivitása: PDAI', array(
      'classes' => 'group-endosc-pdai',
    ));
  case 'group_endosc_montreal':
    return _opr2grp('indent', 'Montreali klasszifikáció');
  case 'group_endosc_mayo':
    return _opr2grp('indent', 'Mayo score');

  case 'group_hyst':
    return _opr2grp('category', '3.4. Szövettan');

  case 'group_imaging':
    return _opr2grp('category', '3.5. Képalkotók');
  case 'group_imaging_abdus':
  case 'group_imaging_abdrtg':
  case 'group_imaging_abdct':
  case 'group_imaging_abdct_ent':
  case 'group_imaging_abdmr':
  case 'group_imaging_abdmr_ent':
  case 'group_imaging_mrcp':
  case 'group_imaging_eus':
    return _opr2grp('conditional', '');

  case 'group_exam':
    return _opr2grp('category', '3.6. Egyéb vizsgálatok');
  case 'group_functexam':
  case 'group_functexam_lact':
  case 'group_functexam_lactu':
  case 'group_functexam_oth':
  case 'group_gentest_pre':
  case 'group_exam_tpmt':
    return _opr2grp('conditional', '');

  case 'group_status':
    return _opr2grp('category', '5. Jelen panaszok, státusz');
  case 'group_status_inline':
    return _opr2grp('inline', '');
  case 'group_admission_rows':
    return _opr2grp('inline', '', array(
      'classes' => 'field-group-multicol group-admission-rows',
    ));
  case 'group_admission_row1':
  case 'group_admission_row2':
  case 'group_admission_row3':
  case 'group_admission_row4':
  case 'group_admission_row5':
  case 'group_admission_row6':
  case 'group_admission_row7':
    return _opr2grp('div', '', array(
      'classes' => 'field-group-multicol-row',
    ));
  case 'group_chestexam':
    return _opr2grp('indent', 'Mellkas fizikális vizsgálata');
  case 'group_abdexam':
    return _opr2grp('indent', 'Has fizikális vizsgálata');
  case 'group_abdcomp':
  case 'group_abdomtender':
  case 'group_abdexam_resist':
  case 'group_abdexam_rdv':
    return _opr2grp('conditional', '');
  case 'group_physdiff':
    return _opr2grp('indent', 'Egyéb fizikális eltérések');

  case 'group_ther':
    return _opr2grp('category', '6. Terápia');
  case 'group_ther_bio':
    return _opr2grp('conditional', '');

  case 'group_int':
    return _opr2grp('category', '4. Intervenciók');
  case 'group_int_surg':
    return _opr2grp('cond_cat', '4.1. Történt-e sebészi beavatkozás?');
  case 'group_int_ibd_ind':
    return _opr2grp('indent', 'Indikáció');
  case 'group_int_ibd':
  case 'group_int_ibd_indfulcol':
  case 'group_int_ibd_indperf':
  case 'group_int_ibd_indbleed':
  case 'group_int_ibd_indthfail':
  case 'group_int_ibd_inddyspl':
  case 'group_int_ibd_indcarc':
  case 'group_int_ibd_indthres':
  case 'group_int_ibd_indobst':
  case 'group_int_ibd_indfist':
  case 'group_int_ibd_inddrain':
  case 'group_int_ibd_indfulm':
    return _opr2grp('conditional', '');
  case 'group_int_end':
    return _opr2grp('cond_cat', '4.2. Történt-e endoszkópos intervenció?');
  case 'group_preg':
    return _opr2grp('category', '7. Terhesség');
  case 'group_preg1':
    return _opr2grp('indent', '');
  case 'group_preg_conabn':
    return _opr2grp('conditional', '');
  case 'group_comp':
    return _opr2grp('category', '8. Szövődmények');
  case 'group_comp_int':
  case 'group_comp_bildu':
  case 'group_complication_orgfail':
  case 'group_therapy_intensive':
  case 'group_complication_death':
  case 'group_comp_other':
    return _opr2grp('conditional', '');
  }
}
